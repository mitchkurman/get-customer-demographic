package com.amazonaws.lambda.rcproject.customerdemographic;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class LambdaFunctionHandler implements RequestHandler<LinkedHashMap<String,String>, String> {

    @Override
    public String handleRequest(LinkedHashMap<String,String> input, Context context) {
        context.getLogger().log("Input: " + input);
        context.getLogger().log("Class Type: "+input.getClass().getName()+"  ");
        
        // TODO: implement your handler
        Demographic demo = new Demographic();
//        Gson gson = new GsonBuilder().create();
//        gson.fromJson(lhm.toString(), demo.getClass());
//        Demographic demo = gson.fromJson(lhm., Demographic.class);
        
//        context.getLogger().log("cardnumber: " + demo.getCardnumber());
        String tmpCN = input.get("cardnumber");
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
        expressionAttributeValues.put(":val1", new AttributeValue(tmpCN));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("cardnumber = :val1").withExpressionAttributeValues(expressionAttributeValues);
        List<Demographic> scanResult = mapper.scan(Demographic.class, scanExpression);
        
        
        System.out.println("this is the input: "+input);
        System.out.println("this is the demographic values: "+demo.toString());
        Gson gson = new GsonBuilder().create();
        
        return gson.toJson(scanResult, PaginatedScanList.class);
    }
    
  


}
