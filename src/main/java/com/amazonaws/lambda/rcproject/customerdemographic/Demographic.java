package com.amazonaws.lambda.rcproject.customerdemographic;


//import java.util.Set;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
//import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName="rc_customers")
public class Demographic {
//	{
//	    "uuid": "7d0c7006-27c2-4cb7-b493-5ae0e3c6037b",
//	    "last-name": "Mohiuddin",
//	    "first-initial": "I",
//	    "concatenated-name": "Mohiuddin I",
//	    "cardnumber": "372545000000014",
//	    "address" : "21 Henry Street Monroe Township, NJ 08831",
//	    "phone": "(200) 691-3694"
//	  }
		public Demographic(){
			
		}
	
	    private String uuid;
	    private String lastName;
	    private String firstInitial;
	    private String concatenatedName;
	    private String cardnumber;
	    private String address;
	    private String phone;
	    
	    @DynamoDBHashKey(attributeName="uuid")  
	    public String getUuid() { return uuid; }
	    public void setUuid(String uuid) {this.uuid = uuid; }
	    
	    @DynamoDBAttribute(attributeName="last-name")  
	    public String getLastName() {return lastName; }
	    public void setLastName(String lastName) { this.lastName = lastName; }
	    
	    @DynamoDBAttribute(attributeName="first-initial")  
	    public String getFirstInitial() { return firstInitial; }
	    public void setFirstInitial(String firstInitial) { this.firstInitial = firstInitial; }
	    
	    @DynamoDBAttribute(attributeName="concatenated-name")
	    public String getConcatenatedName() { return concatenatedName; }
	    public void setConcatenatedName(String concatenatedName) { this.concatenatedName = concatenatedName; }
	    
	    @DynamoDBAttribute(attributeName="cardnumber")
	    public String getCardnumber() { return cardnumber; }
	    public void setCardnumber(String cardnumber) { this.cardnumber = cardnumber; }
	    
	    @DynamoDBAttribute(attributeName="address")
	    public String getAddress() { return address; }
	    public void setAddress(String address) { this.address = address; }
	    
	    @DynamoDBAttribute(attributeName="phone")
	    public String getPhone() { return phone; }
	    public void setPhone(String phone) { this.phone = phone; }
	    
//	    @DynamoDBIgnore
//	    public String getSomeProp() { return someProp; }
//	    public void setSomeProp(String someProp) { this.someProp = someProp; }
	    
	    public String toString(){
	    	return new StringBuffer("uuid :"+getUuid()+ " lastName: "+getLastName() +" ...").toString();
	    }

}
